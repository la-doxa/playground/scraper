/*
 * uData API
 * uData API
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.asso.ladoxa.datagouv.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Job
 */

public class Job {
  @JsonProperty("args")
  private List<Object> args = null;

  @JsonProperty("crontab")
  private Crontab crontab = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("enabled")
  private Boolean enabled = false;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("interval")
  private Interval interval = null;

  @JsonProperty("kwargs")
  private Object kwargs = null;

  @JsonProperty("last_run_at")
  private LocalDateTime lastRunAt = null;

  @JsonProperty("last_run_id")
  private String lastRunId = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("schedule")
  private String schedule = null;

  /**
   * The task name
   */
  public enum TaskEnum {
    TEST_HIGH_QUEUE("test-high-queue"),

    TEST_DEFAULT_QUEUE("test-default-queue"),

    EXPORT_CSV("export-csv"),

    PIWIK_CURRENT_METRICS("piwik-current-metrics"),

    PURGE_REUSES("purge-reuses"),

    PIWIK_YESTERDAY_METRICS("piwik-yesterday-metrics"),

    TEST_ERROR("test-error"),

    PURGE_CHUNKS("purge-chunks"),

    PURGE_HARVESTERS("purge-harvesters"),

    PURGE_DATASETS("purge-datasets"),

    PURGE_HARVEST_JOBS("purge-harvest-jobs"),

    PIWIK_BULK_TRACK_API("piwik-bulk-track-api"),

    COMPUTE_SITE_METRICS("compute-site-metrics"),

    SEND_FREQUENCY_REMINDER("send-frequency-reminder"),

    TEST_LOG("test-log"),

    HARVEST("harvest"),

    PURGE_ORGANIZATIONS("purge-organizations"),

    TEST_LOW_QUEUE("test-low-queue"),

    PIWIK_UPDATE_METRICS("piwik-update-metrics"),

    COUNT_TAGS("count-tags");

    private String value;

    TaskEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TaskEnum fromValue(String text) {
      for (TaskEnum b : TaskEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("task")
  private TaskEnum task = null;

  public Job args(List<Object> args) {
    this.args = args;
    return this;
  }

  public Job addArgsItem(Object argsItem) {
    if (this.args == null) {
      this.args = new ArrayList<Object>();
    }
    this.args.add(argsItem);
    return this;
  }

   /**
   * The job execution arguments
   * @return args
  **/
  @ApiModelProperty(value = "The job execution arguments")
  public List<Object> getArgs() {
    return args;
  }

  public void setArgs(List<Object> args) {
    this.args = args;
  }

  public Job crontab(Crontab crontab) {
    this.crontab = crontab;
    return this;
  }

   /**
   * Get crontab
   * @return crontab
  **/
  @ApiModelProperty(value = "")
  public Crontab getCrontab() {
    return crontab;
  }

  public void setCrontab(Crontab crontab) {
    this.crontab = crontab;
  }

  public Job description(String description) {
    this.description = description;
    return this;
  }

   /**
   * The job description
   * @return description
  **/
  @ApiModelProperty(value = "The job description")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Job enabled(Boolean enabled) {
    this.enabled = enabled;
    return this;
  }

   /**
   * Is this job enabled
   * @return enabled
  **/
  @ApiModelProperty(value = "Is this job enabled")
  public Boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

   /**
   * The job unique identifier
   * @return id
  **/
  @ApiModelProperty(value = "The job unique identifier")
  public String getId() {
    return id;
  }

  public Job interval(Interval interval) {
    this.interval = interval;
    return this;
  }

   /**
   * Get interval
   * @return interval
  **/
  @ApiModelProperty(value = "")
  public Interval getInterval() {
    return interval;
  }

  public void setInterval(Interval interval) {
    this.interval = interval;
  }

  public Job kwargs(Object kwargs) {
    this.kwargs = kwargs;
    return this;
  }

   /**
   * The job execution keyword arguments
   * @return kwargs
  **/
  @ApiModelProperty(value = "The job execution keyword arguments")
  public Object getKwargs() {
    return kwargs;
  }

  public void setKwargs(Object kwargs) {
    this.kwargs = kwargs;
  }

   /**
   * The last job execution date
   * @return lastRunAt
  **/
  @ApiModelProperty(value = "The last job execution date")
  public LocalDateTime getLastRunAt() {
    return lastRunAt;
  }

   /**
   * The last execution task id
   * @return lastRunId
  **/
  @ApiModelProperty(value = "The last execution task id")
  public String getLastRunId() {
    return lastRunId;
  }

  public Job name(String name) {
    this.name = name;
    return this;
  }

   /**
   * The job unique name
   * @return name
  **/
  @ApiModelProperty(required = true, value = "The job unique name")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

   /**
   * The schedule display
   * @return schedule
  **/
  @ApiModelProperty(value = "The schedule display")
  public String getSchedule() {
    return schedule;
  }

  public Job task(TaskEnum task) {
    this.task = task;
    return this;
  }

   /**
   * The task name
   * @return task
  **/
  @ApiModelProperty(example = "test-high-queue", required = true, value = "The task name")
  public TaskEnum getTask() {
    return task;
  }

  public void setTask(TaskEnum task) {
    this.task = task;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Job job = (Job) o;
    return Objects.equals(this.args, job.args) &&
        Objects.equals(this.crontab, job.crontab) &&
        Objects.equals(this.description, job.description) &&
        Objects.equals(this.enabled, job.enabled) &&
        Objects.equals(this.id, job.id) &&
        Objects.equals(this.interval, job.interval) &&
        Objects.equals(this.kwargs, job.kwargs) &&
        Objects.equals(this.lastRunAt, job.lastRunAt) &&
        Objects.equals(this.lastRunId, job.lastRunId) &&
        Objects.equals(this.name, job.name) &&
        Objects.equals(this.schedule, job.schedule) &&
        Objects.equals(this.task, job.task);
  }

  @Override
  public int hashCode() {
    return Objects.hash(args, crontab, description, enabled, id, interval, kwargs, lastRunAt, lastRunId, name, schedule, task);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Job {\n");

    sb.append("    args: ").append(toIndentedString(args)).append("\n");
    sb.append("    crontab: ").append(toIndentedString(crontab)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    enabled: ").append(toIndentedString(enabled)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    interval: ").append(toIndentedString(interval)).append("\n");
    sb.append("    kwargs: ").append(toIndentedString(kwargs)).append("\n");
    sb.append("    lastRunAt: ").append(toIndentedString(lastRunAt)).append("\n");
    sb.append("    lastRunId: ").append(toIndentedString(lastRunId)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    schedule: ").append(toIndentedString(schedule)).append("\n");
    sb.append("    task: ").append(toIndentedString(task)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

