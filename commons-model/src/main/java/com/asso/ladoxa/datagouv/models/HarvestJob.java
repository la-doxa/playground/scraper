/*
 * uData API
 * uData API
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.asso.ladoxa.datagouv.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModelProperty;
import java.time.LocalDateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * HarvestJob
 */

public class HarvestJob {
  @JsonProperty("created")
  private LocalDateTime created = null;

  @JsonProperty("ended")
  private LocalDateTime ended = null;

  @JsonProperty("errors")
  private List<HarvestError> errors = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("items")
  private List<HarvestItem> items = null;

  @JsonProperty("source")
  private String source = null;

  @JsonProperty("started")
  private LocalDateTime started = null;

  /**
   * The job status
   */
  public enum StatusEnum {
    PENDING("pending"),

    INITIALIZING("initializing"),

    INITIALIZED("initialized"),

    PROCESSING("processing"),

    DONE("done"),

    DONE_ERRORS("done-errors"),

    FAILED("failed");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String text) {
      for (StatusEnum b : StatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("status")
  private StatusEnum status = null;

  public HarvestJob created(LocalDateTime created) {
    this.created = created;
    return this;
  }

   /**
   * The job creation date
   * @return created
  **/
  @ApiModelProperty(required = true, value = "The job creation date")
  public LocalDateTime getCreated() {
    return created;
  }

  public void setCreated(LocalDateTime created) {
    this.created = created;
  }

  public HarvestJob ended(LocalDateTime ended) {
    this.ended = ended;
    return this;
  }

   /**
   * The job end date
   * @return ended
  **/
  @ApiModelProperty(value = "The job end date")
  public LocalDateTime getEnded() {
    return ended;
  }

  public void setEnded(LocalDateTime ended) {
    this.ended = ended;
  }

  public HarvestJob errors(List<HarvestError> errors) {
    this.errors = errors;
    return this;
  }

  public HarvestJob addErrorsItem(HarvestError errorsItem) {
    if (this.errors == null) {
      this.errors = new ArrayList<HarvestError>();
    }
    this.errors.add(errorsItem);
    return this;
  }

   /**
   * The job initialization errors
   * @return errors
  **/
  @ApiModelProperty(value = "The job initialization errors")
  public List<HarvestError> getErrors() {
    return errors;
  }

  public void setErrors(List<HarvestError> errors) {
    this.errors = errors;
  }

  public HarvestJob id(String id) {
    this.id = id;
    return this;
  }

   /**
   * The job execution ID
   * @return id
  **/
  @ApiModelProperty(required = true, value = "The job execution ID")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public HarvestJob items(List<HarvestItem> items) {
    this.items = items;
    return this;
  }

  public HarvestJob addItemsItem(HarvestItem itemsItem) {
    if (this.items == null) {
      this.items = new ArrayList<HarvestItem>();
    }
    this.items.add(itemsItem);
    return this;
  }

   /**
   * The job collected items
   * @return items
  **/
  @ApiModelProperty(value = "The job collected items")
  public List<HarvestItem> getItems() {
    return items;
  }

  public void setItems(List<HarvestItem> items) {
    this.items = items;
  }

  public HarvestJob source(String source) {
    this.source = source;
    return this;
  }

   /**
   * The source owning the job
   * @return source
  **/
  @ApiModelProperty(required = true, value = "The source owning the job")
  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public HarvestJob started(LocalDateTime started) {
    this.started = started;
    return this;
  }

   /**
   * The job start date
   * @return started
  **/
  @ApiModelProperty(value = "The job start date")
  public LocalDateTime getStarted() {
    return started;
  }

  public void setStarted(LocalDateTime started) {
    this.started = started;
  }

  public HarvestJob status(StatusEnum status) {
    this.status = status;
    return this;
  }

   /**
   * The job status
   * @return status
  **/
  @ApiModelProperty(example = "pending", required = true, value = "The job status")
  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HarvestJob harvestJob = (HarvestJob) o;
    return Objects.equals(this.created, harvestJob.created) &&
        Objects.equals(this.ended, harvestJob.ended) &&
        Objects.equals(this.errors, harvestJob.errors) &&
        Objects.equals(this.id, harvestJob.id) &&
        Objects.equals(this.items, harvestJob.items) &&
        Objects.equals(this.source, harvestJob.source) &&
        Objects.equals(this.started, harvestJob.started) &&
        Objects.equals(this.status, harvestJob.status);
  }

  @Override
  public int hashCode() {
    return Objects.hash(created, ended, errors, id, items, source, started, status);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class HarvestJob {\n");

    sb.append("    created: ").append(toIndentedString(created)).append("\n");
    sb.append("    ended: ").append(toIndentedString(ended)).append("\n");
    sb.append("    errors: ").append(toIndentedString(errors)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    items: ").append(toIndentedString(items)).append("\n");
    sb.append("    source: ").append(toIndentedString(source)).append("\n");
    sb.append("    started: ").append(toIndentedString(started)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

