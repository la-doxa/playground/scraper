/*
 * uData API
 * uData API
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.asso.ladoxa.datagouv.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * UserSuggestion
 */

public class UserSuggestion {
  @JsonProperty("avatar_url")
  private String avatarUrl = null;

  @JsonProperty("first_name")
  private String firstName = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("last_name")
  private String lastName = null;

  @JsonProperty("score")
  private BigDecimal score = null;

  @JsonProperty("slug")
  private String slug = null;

  public UserSuggestion avatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
    return this;
  }

   /**
   * The user avatar URL
   * @return avatarUrl
  **/
  @ApiModelProperty(value = "The user avatar URL")
  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

   /**
   * The user first name
   * @return firstName
  **/
  @ApiModelProperty(value = "The user first name")
  public String getFirstName() {
    return firstName;
  }

   /**
   * The user identifier
   * @return id
  **/
  @ApiModelProperty(value = "The user identifier")
  public String getId() {
    return id;
  }

   /**
   * The user last name
   * @return lastName
  **/
  @ApiModelProperty(value = "The user last name")
  public String getLastName() {
    return lastName;
  }

   /**
   * The internal match score
   * @return score
  **/
  @ApiModelProperty(value = "The internal match score")
  public BigDecimal getScore() {
    return score;
  }

   /**
   * The user permalink string
   * @return slug
  **/
  @ApiModelProperty(value = "The user permalink string")
  public String getSlug() {
    return slug;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserSuggestion userSuggestion = (UserSuggestion) o;
    return Objects.equals(this.avatarUrl, userSuggestion.avatarUrl) &&
        Objects.equals(this.firstName, userSuggestion.firstName) &&
        Objects.equals(this.id, userSuggestion.id) &&
        Objects.equals(this.lastName, userSuggestion.lastName) &&
        Objects.equals(this.score, userSuggestion.score) &&
        Objects.equals(this.slug, userSuggestion.slug);
  }

  @Override
  public int hashCode() {
    return Objects.hash(avatarUrl, firstName, id, lastName, score, slug);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserSuggestion {\n");

    sb.append("    avatarUrl: ").append(toIndentedString(avatarUrl)).append("\n");
    sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
    sb.append("    score: ").append(toIndentedString(score)).append("\n");
    sb.append("    slug: ").append(toIndentedString(slug)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

