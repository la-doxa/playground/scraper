/*
 * uData API
 * uData API
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.asso.ladoxa.datagouv.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * MyMetrics
 */

public class MyMetrics {
  @JsonProperty("datasets_count")
  private Integer datasetsCount = null;

  @JsonProperty("datasets_org_count")
  private Integer datasetsOrgCount = null;

  @JsonProperty("followers_count")
  private Integer followersCount = null;

  @JsonProperty("followers_org_count")
  private Integer followersOrgCount = null;

  @JsonProperty("id")
  private String id = null;

  @JsonProperty("resources_availability")
  private BigDecimal resourcesAvailability = null;

   /**
   * The user&#39;s datasets number
   * @return datasetsCount
  **/
  @ApiModelProperty(value = "The user's datasets number")
  public Integer getDatasetsCount() {
    return datasetsCount;
  }

   /**
   * The user&#39;s orgs datasets number
   * @return datasetsOrgCount
  **/
  @ApiModelProperty(value = "The user's orgs datasets number")
  public Integer getDatasetsOrgCount() {
    return datasetsOrgCount;
  }

   /**
   * The user&#39;s followers number
   * @return followersCount
  **/
  @ApiModelProperty(value = "The user's followers number")
  public Integer getFollowersCount() {
    return followersCount;
  }

   /**
   * The user&#39;s orgs followers number
   * @return followersOrgCount
  **/
  @ApiModelProperty(value = "The user's orgs followers number")
  public Integer getFollowersOrgCount() {
    return followersOrgCount;
  }

  public MyMetrics id(String id) {
    this.id = id;
    return this;
  }

   /**
   * The user identifier
   * @return id
  **/
  @ApiModelProperty(required = true, value = "The user identifier")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

   /**
   * The user&#39;s resources availability percentage
   * @return resourcesAvailability
  **/
  @ApiModelProperty(value = "The user's resources availability percentage")
  public BigDecimal getResourcesAvailability() {
    return resourcesAvailability;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MyMetrics myMetrics = (MyMetrics) o;
    return Objects.equals(this.datasetsCount, myMetrics.datasetsCount) &&
        Objects.equals(this.datasetsOrgCount, myMetrics.datasetsOrgCount) &&
        Objects.equals(this.followersCount, myMetrics.followersCount) &&
        Objects.equals(this.followersOrgCount, myMetrics.followersOrgCount) &&
        Objects.equals(this.id, myMetrics.id) &&
        Objects.equals(this.resourcesAvailability, myMetrics.resourcesAvailability);
  }

  @Override
  public int hashCode() {
    return Objects.hash(datasetsCount, datasetsOrgCount, followersCount, followersOrgCount, id, resourcesAvailability);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MyMetrics {\n");

    sb.append("    datasetsCount: ").append(toIndentedString(datasetsCount)).append("\n");
    sb.append("    datasetsOrgCount: ").append(toIndentedString(datasetsOrgCount)).append("\n");
    sb.append("    followersCount: ").append(toIndentedString(followersCount)).append("\n");
    sb.append("    followersOrgCount: ").append(toIndentedString(followersOrgCount)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    resourcesAvailability: ").append(toIndentedString(resourcesAvailability)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

