/*
 * uData API
 * uData API
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.asso.ladoxa.datagouv.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

/**
 * OrganizationReference
 */

public class OrganizationReference extends BaseReference {
  @JsonProperty("acronym")
  private String acronym = null;

  @JsonProperty("logo")
  private String logo = null;

  @JsonProperty("logo_thumbnail")
  private String logoThumbnail = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("page")
  private String page = null;

  @JsonProperty("slug")
  private String slug = null;

  @JsonProperty("uri")
  private String uri = null;

  public OrganizationReference acronym(String acronym) {
    this.acronym = acronym;
    return this;
  }

   /**
   * The organization acronym
   * @return acronym
  **/
  @ApiModelProperty(value = "The organization acronym")
  public String getAcronym() {
    return acronym;
  }

  public void setAcronym(String acronym) {
    this.acronym = acronym;
  }

  public OrganizationReference logo(String logo) {
    this.logo = logo;
    return this;
  }

   /**
   * The organization logo URL
   * @return logo
  **/
  @ApiModelProperty(value = "The organization logo URL")
  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public OrganizationReference logoThumbnail(String logoThumbnail) {
    this.logoThumbnail = logoThumbnail;
    return this;
  }

   /**
   * The organization logo thumbnail URL. This is the square (100x100) and cropped version.
   * @return logoThumbnail
  **/
  @ApiModelProperty(value = "The organization logo thumbnail URL. This is the square (100x100) and cropped version.")
  public String getLogoThumbnail() {
    return logoThumbnail;
  }

  public void setLogoThumbnail(String logoThumbnail) {
    this.logoThumbnail = logoThumbnail;
  }

   /**
   * The organization name
   * @return name
  **/
  @ApiModelProperty(value = "The organization name")
  public String getName() {
    return name;
  }

   /**
   * The organization web page URL
   * @return page
  **/
  @ApiModelProperty(value = "The organization web page URL")
  public String getPage() {
    return page;
  }

  public OrganizationReference slug(String slug) {
    this.slug = slug;
    return this;
  }

   /**
   * The organization string used as permalink
   * @return slug
  **/
  @ApiModelProperty(required = true, value = "The organization string used as permalink")
  public String getSlug() {
    return slug;
  }

  public void setSlug(String slug) {
    this.slug = slug;
  }

   /**
   * The organization API URI
   * @return uri
  **/
  @ApiModelProperty(value = "The organization API URI")
  public String getUri() {
    return uri;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OrganizationReference organizationReference = (OrganizationReference) o;
    return Objects.equals(this.acronym, organizationReference.acronym) &&
        Objects.equals(this.logo, organizationReference.logo) &&
        Objects.equals(this.logoThumbnail, organizationReference.logoThumbnail) &&
        Objects.equals(this.name, organizationReference.name) &&
        Objects.equals(this.page, organizationReference.page) &&
        Objects.equals(this.slug, organizationReference.slug) &&
        Objects.equals(this.uri, organizationReference.uri) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(acronym, logo, logoThumbnail, name, page, slug, uri, super.hashCode());
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OrganizationReference {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    acronym: ").append(toIndentedString(acronym)).append("\n");
    sb.append("    logo: ").append(toIndentedString(logo)).append("\n");
    sb.append("    logoThumbnail: ").append(toIndentedString(logoThumbnail)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    page: ").append(toIndentedString(page)).append("\n");
    sb.append("    slug: ").append(toIndentedString(slug)).append("\n");
    sb.append("    uri: ").append(toIndentedString(uri)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

