package com.asso.ladoxa.assemblee.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Organe {
    @JsonProperty("organe_uid")
    private String uid;

    private String codeType;
    private String libelle;
    private String libelleEdition;
    private String libelleAbrege;
    private String libelleAbrev;
    private String organeParent;
    private String chambre;
    private String regime;
    private String legislature;

    private String dateDebut;
    private String dateAgrement;
    private String dateFin;
    private List<String> secretariat = new ArrayList<>();

    @JsonProperty("uid")
    private void parseUID(String uid) {
        this.setUid(uid);
    }


    @JsonProperty("viMoDe")
    private void parseViMoDe(Map<String, String> viMoDe) {
        this.setDateDebut(viMoDe.get("dateDebut"));
        this.setDateAgrement(viMoDe.get("dateAgrement"));
        this.setDateFin(viMoDe.get("dateFin"));
    }

    @JsonProperty("secretariat")
    private void parseSecretariat(Map<String, String> secretariat) {
        this.getSecretariat().add(secretariat.get("secretaire01"));
        this.getSecretariat().add(secretariat.get("secretaire02"));
    }

}
