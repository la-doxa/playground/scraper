package com.asso.ladoxa.assemblee.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class Legislature {

    private String legislature;
    private Map<String, Object> profession;
    @JsonProperty("uri_hatvp")
    private String uri;
    private List<Map<String, Object>> adresses;
    private List<Map<String, Object>> mandats;
    private Organe organe;
}
