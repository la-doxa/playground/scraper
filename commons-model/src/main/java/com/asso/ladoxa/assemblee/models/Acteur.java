package com.asso.ladoxa.assemblee.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Acteur {
    @JsonProperty("depute_uid")
    private String uid;
    private String civilite;
    private String name;
    private String surname;
    private String alpha;
    private String trigramme;

    @JsonProperty("birth_date")
    private String birthDate;

    @JsonProperty("birth_city")
    private String birthCity;

    @JsonProperty("birth_department")
    private String birthDepartment;

    @JsonProperty("birth_country")
    private String birthCountry;

    private Map<String, Object> profession;

    @JsonProperty("uri_hatvp")
    private String uri;
    private List<Map<String, Object>> adresses = new ArrayList<>();
    private List<Map<String, Object>> mandats = new ArrayList<>();

    @JsonProperty("uid")
    private void unpackUID(Map<String, String> uid) {
        this.setUid(uid.get("#text"));
    }

    @JsonProperty("etatCivil")
    private void parsePersonInfo(Object obj) {
        Map<String, Map<String,String>> etatCivil = (Map<String, Map<String,String>>) obj;
        this.setCivilite(etatCivil.get("ident").get("civ"));
        this.setName(etatCivil.get("ident").get("nom"));
        this.setSurname(etatCivil.get("ident").get("prenom"));
        this.setAlpha(etatCivil.get("ident").get("alpha"));
        this.setTrigramme(etatCivil.get("ident").get("trigramme"));

        this.setBirthDate(etatCivil.get("infoNaissance").get("dateNais"));
        this.setBirthCity(etatCivil.get("infoNaissance").get("villeNais"));
        this.setBirthDepartment(etatCivil.get("infoNaissance").get("depNais"));
        this.setBirthCountry(etatCivil.get("infoNaissance").get("paysNais"));
    }

    @JsonProperty("adresses")
    private void parseAdresses(Object obj) {
        Map<String, Object> adresses = (Map<String, Object>) obj;
        if (adresses.get("adresse") instanceof List) {
            List<Map<String, Object>> adressesList = (List<Map<String, Object>>) adresses.get("adresse");
            for(Map<String, Object> entry : adressesList) {
                replaceUid("adresse_uid", entry);
            }
            this.setAdresses((List<Map<String, Object>>) adresses.get("adresse"));
        } else {
            Map<String, Object> adresse = (Map<String, Object>)adresses.get("adresse");
            replaceUid("adresse_uid", adresse);
            this.getAdresses().add((Map<String, Object>)adresses.get("adresse"));
        }
    }

    @JsonProperty("mandats")
    private void parseMandats(Object obj) {
        Map<String, Object> mandats = (Map<String, Object>) obj;
        if (mandats.get("mandat") instanceof List) {
            List<Map<String, Object>> mandatsList = (List<Map<String, Object>>) mandats.get("mandat");
            for(Map<String, Object> entry : mandatsList) {
                replaceUid("mandat_uid", entry);
                replaceOrganeRef("organe_uid", entry);
            }
            this.setMandats((List<Map<String, Object>>) mandats.get("mandat"));
        } else {
            Map<String, Object> mandat = (Map<String, Object>)mandats.get("mandat");
            replaceUid("mandat_uid", mandat);
            replaceOrganeRef("organe_uid", mandat);
            this.getMandats().add(mandat);
        }
    }

    private void replaceUid(String newKey, Map<String, Object> obj) {
        obj.put(newKey, obj.get("uid"));
        obj.remove("uid");
    }

    private void replaceOrganeRef(String newKey, Map<String, Object> obj) {
        obj.put(newKey, ((Map<String, String>) obj.get("organes")).get("organeRef"));
        obj.remove("organes");
    }
}
