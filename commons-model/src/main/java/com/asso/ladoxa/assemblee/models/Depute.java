package com.asso.ladoxa.assemblee.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
public class Depute {
    @JsonProperty("depute_uid")
    private String uid;
    private String civilite;
    private String name;
    private String surname;
    private String alpha;
    private String trigramme;

    @JsonProperty("birth_date")
    private String birthDate;

    @JsonProperty("birth_city")
    private String birthCity;

    @JsonProperty("birth_department")
    private String birthDepartment;

    @JsonProperty("birth_country")
    private String birthCountry;

    private List<Legislature> legislatures = new ArrayList<>();
}
