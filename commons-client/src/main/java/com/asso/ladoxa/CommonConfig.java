package com.asso.ladoxa;

import com.fasterxml.jackson.datatype.joda.JodaMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Configuration
@ComponentScan
public class CommonConfig {
    @Bean
    public JodaMapper mapper() {
        JodaMapper mapper = new JodaMapper();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm a z");
        mapper.setDateFormat(df);
        mapper.findAndRegisterModules();
        return mapper;
    }
}
