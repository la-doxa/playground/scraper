package com.asso.ladoxa.elasticsearch;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.datatype.joda.JodaMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.collapse.CollapseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.List;

@Slf4j
@Repository
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ElasticsearchRepository {

    private final RestHighLevelClient esClient;
    private final  JodaMapper mapper;

    public Mono<GetResponse> findById(String index, String id) {
        return Mono.create(sink -> {
            GetRequest request = new GetRequest()
                    .index(index)
                    .id(id);

            esClient.getAsync(request, RequestOptions.DEFAULT, new ActionListener<>() {
                @Override
                public void onResponse(GetResponse getResponse) {
                    sink.success(getResponse);
                }

                @Override
                public void onFailure(Exception e) {
                    sink.error(e);
                }
            });
        });
    }

    public Mono<UpdateResponse> upsertMono(String index, String id, String source) {
        return Mono.create(sink -> {
            UpdateRequest request = new UpdateRequest()
                    .index(index)
                    .id(id)
                    .type("_doc") // should not ask type but it does :(
                    .docAsUpsert(true)
                    .fetchSource(true)
                    .doc(source, XContentType.JSON);

            esClient.updateAsync(request, RequestOptions.DEFAULT, new ActionListener<>() {
                @Override
                public void onResponse(UpdateResponse updateResponse) {
                    sink.success(updateResponse);
                }

                @Override
                public void onFailure(Exception e) {
                    sink.error(e);
                }
            });
        });
    }

    public Mono<IndexRequest> buildIndexRequest(String index, String id, Object source) {
        IndexRequest request = null;
        try {
            request = new IndexRequest()
                    .index(index)
                    .id(id)
                    .source(mapper.writeValueAsString(source), XContentType.JSON);

        } catch (JsonProcessingException e) {
            Mono.error(e);
        }
        return Mono.just(request);
    }

    public Mono<BulkRequest> buildBulkRequest(List<IndexRequest> requests)  {
        BulkRequest bulkRequest = new BulkRequest();
        requests.stream()
                .forEach(request -> {
                    bulkRequest.add(request);
                });
        return Mono.just(bulkRequest);
    }

    public Mono<BulkResponse> bulk(BulkRequest request) {
        return Mono.create(sink -> {
            esClient.bulkAsync(request, RequestOptions.DEFAULT, new ActionListener<>() {
                @Override
                public void onResponse(BulkResponse response) {
                    if(response.hasFailures()) sink.error(new Exception("Bulk has failures"));
                    sink.success(response);
                }

                @Override
                public void onFailure(Exception e) {
                    sink.error(e);
                }
            });
        });
    }

    /**
     * Simple scroll search with QueryBuilder and 'null' collapse / aggregation
     * @param index             Elasticsearch index
     * @param scrollId          ScrollId received after first scroll search iteration (nullable)
     * @param includeFields     Fields to include
     * @param excludeFields     Fields to exclude
     * @param size              Number of elements to return
     * @return  the {@link SearchResponse}
     */
    public Mono<SearchResponse> scrollSearch(String index, String scrollId, String[] includeFields,
                                             String[] excludeFields, int size) {
        return scrollSearch(index, scrollId, includeFields, excludeFields, size, QueryBuilders.matchAllQuery(), null);
    }

    /**
     * Scroll search with QueryBuilder and 'null' collapse / aggregation
     * @param index             Elasticsearch index
     * @param scrollId          ScrollId received after first scroll search iteration (nullable)
     * @param includeFields     Fields to include
     * @param excludeFields     Fields to exclude
     * @param size              Number of elements to return
     * @param queryBuilder      Elasticsearch {@link QueryBuilder}
     * @return  the {@link SearchResponse}
     */
    public Mono<SearchResponse> scrollSearch(String index, String scrollId, String[] includeFields,
                                             String[] excludeFields, int size, QueryBuilder queryBuilder) {
        return scrollSearch(index, scrollId, includeFields, excludeFields, size, queryBuilder, null);
    }

    /**
     * Full scroll search
     * @param index             Elasticsearch index
     * @param scrollId          ScrollId received after first scroll search iteration (nullable)
     * @param includeFields     Fields to include
     * @param excludeFields     Fields to exclude
     * @param size              Number of elements to return
     * @param queryBuilder      Elasticsearch {@link QueryBuilder}
     * @param collapseBuilder   Elasticsearch {@link CollapseBuilder}
     * @return  the {@link SearchResponse}
     */
    public Mono<SearchResponse> scrollSearch(String index, String scrollId, String[] includeFields,
                                             String[] excludeFields, int size, QueryBuilder queryBuilder,
                                             CollapseBuilder collapseBuilder) {
        if (scrollId == null) {
            SearchRequest searchRequest = new SearchRequest(index);
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchSourceBuilder.query(queryBuilder);
            searchSourceBuilder.collapse(collapseBuilder);
            searchSourceBuilder.size(size);
            searchSourceBuilder.fetchSource(includeFields, excludeFields);
            searchRequest.source(searchSourceBuilder);
            searchRequest.scroll(TimeValue.timeValueMinutes(1L));
            return Mono.create(sink -> {
                esClient.searchAsync(searchRequest, RequestOptions.DEFAULT, new ActionListener<>() {
                    @Override
                    public void onResponse(SearchResponse searchResponse) {
                        sink.success(searchResponse);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        sink.error(e);
                    }
                });
            });
        } else {
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(TimeValue.timeValueSeconds(30));
            return Mono.create(sink -> {
                esClient.scrollAsync(scrollRequest, RequestOptions.DEFAULT, new ActionListener<>() {
                    @Override
                    public void onResponse(SearchResponse searchResponse) {
                        sink.success(searchResponse);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        sink.error(e);
                    }
                });
            });
        }
    }

    public UpdateResponse upsert(String index, String id, String source) throws IOException {
        UpdateRequest request = new UpdateRequest()
                .index(index)
                .id(id)
                .type("_doc") // should not ask type but it does :(
                .docAsUpsert(true)
                .fetchSource(true)
                .doc(source, XContentType.JSON);
        return esClient.update(request, RequestOptions.DEFAULT);
    }
}
