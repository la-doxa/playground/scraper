package com.asso.ladoxa.opentracing;

import io.opentracing.Span;
import io.opentracing.Tracer;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Aspect
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TracerAspect {

    private final Tracer tracer;

    /**
     * LIMITATIONS:
     * This relies on SPRING AOP Project. You cannot track calls from a bean to itself because it bypasses
     * Spring proxies.
     * https://stackoverflow.com/questions/13564627/spring-aop-not-working-for-method-call-inside-another-method
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Around("@annotation(NewSpan)")
    public Object traceMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        String spanName = methodSignature.getMethod().getName();
        Span span = tracer
            .buildSpan(spanName)
            .asChildOf(tracer.activeSpan())
            .start();
        Object result = joinPoint.proceed();
        if (result instanceof Mono<?>) {
            Mono<?> monoResult = (Mono<?>) result;
            return monoResult.doOnTerminate(span::finish);
        } else if (result instanceof Flux<?>) {
            Flux<?> fluxResult = (Flux<?>) result;
            return fluxResult.doOnTerminate(span::finish);
        } else {
            span.finish();
            return result;
        }
    }
}
