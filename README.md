# Scraper 

Scrape data sources metadata to get last modifications and notify it to consumers/subscribers

## Data sources

- [Data Gouv API](https://doc.data.gouv.fr/api/reference/)
- [Assemblée Nationale](https://www.assemblee-nationale.fr/)
- [20 minutes](https://www.20minutes.fr/)

# Webserver

Login to get notified by new modifications

# Deployment local version

1. Build images 
```bash
$ DOCKER_REGISTRY=registry.gitlab.com/la-doxa/playground/scraper DOCKER_TAG={your_tag} mvn package jib:build
```

2. Deploy
```
$ ES_PASSWORD=""
$ helm dependency update ./deployment/helm/scraper && helm upgrade --install --set env.ES_PASSWORD=${ES_PASSWORD} scraper ./deployment/helm/scraper/
```

3. Access
```
$ kubectl port-forward 8081
```

# Production setup
```
$ ES_PASSWORD=""
$ helm dependency update ./deployment/helm/scraper
$ helm upgrade --install --set env.ES_PASSWORD=${ES_PASSWORD},ingress.enabled=true,volumeMounts=true,env.ES_APM_SECRET=${ES_APM_SECRET} scraper ./deployment/helm/scraper/
```
