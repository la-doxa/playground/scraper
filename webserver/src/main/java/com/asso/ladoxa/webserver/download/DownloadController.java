package com.asso.ladoxa.webserver.download;

import co.elastic.apm.api.ElasticApm;
import com.asso.ladoxa.webserver.WebServerApplicationConfig;
import com.asso.ladoxa.webserver.download.models.DownloadEvent;
import com.asso.ladoxa.webserver.download.models.LoggingEvent;
import com.asso.ladoxa.webserver.subscribes.models.ScrollPage;
import com.asso.ladoxa.webserver.upload.UploadService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.message.ObjectMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Log4j2
@RestController
@RequestMapping("/downloads")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DownloadController {

    private final DownloadService downloadService;
    private final UploadService uploadService;
    private final WebServerApplicationConfig applicationConfig;

    @PostMapping
    public Mono<DownloadEvent> download(@RequestBody DownloadEvent downloadEvent, ServerHttpResponse response) {
        downloadEvent.setTraceId(ElasticApm.currentTransaction().getTraceId());
        log.info(downloadEvent.asLoggingEvent("Download request"));
        if (Objects.isNull(downloadEvent.getFileName()) || downloadEvent.getFileName().isEmpty()
            || downloadEvent.getFileName().isBlank()) {
            response.setStatusCode(HttpStatus.BAD_REQUEST);
            downloadEvent.setError("Missing file_name");
            return Mono.just(downloadEvent);
        }
        return downloadService.download(downloadEvent)
            .onErrorResume(throwable -> {
                downloadEvent.setError(throwable.getMessage());
                return Mono.just(downloadEvent);
            })
            .flatMap(uploadService::upload)
            .doOnSuccess(downloadEvent1 -> log.info(new ObjectMessage(LoggingEvent.builder()
                .message("Download ended").payload(downloadEvent1).build())))
            .onErrorResume(throwable -> {
                log.error(downloadEvent.asLoggingEvent(throwable.getMessage()));
                downloadEvent.setError(throwable.getMessage());
                return Mono.just(downloadEvent);
            });
    }

    @GetMapping
    public Mono<ScrollPage<DownloadEvent>> downloads(@RequestParam(name = "pageId", required = false) String pageId,
                                               @RequestParam(name = "size", required = false) Integer size) {
        int appliedSize = size == null ? applicationConfig.getDefaultScrollSize() : size;
        return downloadService.findAll(pageId, appliedSize, new String[] { },
            new String[]{});
    }
}
