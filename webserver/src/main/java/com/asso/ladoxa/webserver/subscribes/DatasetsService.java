package com.asso.ladoxa.webserver.subscribes;

import com.asso.ladoxa.datagouv.models.Dataset;
import com.asso.ladoxa.elasticsearch.ElasticsearchRepository;
import com.asso.ladoxa.webserver.WebServerApplicationConfig;
import com.asso.ladoxa.webserver.subscribes.models.ScrollPage;
import com.fasterxml.jackson.datatype.joda.JodaMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
@Service
public class DatasetsService {

    protected WebServerApplicationConfig appConfig;
    protected ElasticsearchRepository esRepository;
    protected JodaMapper mapper;

    @Autowired
    public DatasetsService(ElasticsearchRepository esRepository, JodaMapper mapper,
                           WebServerApplicationConfig webServerApplicationConfig) {
        this.esRepository = esRepository;
        this.mapper = mapper;
        this.appConfig = webServerApplicationConfig;
    }

    public Dataset toDataset(Object obj) {
        try {
            return mapper.readValue(mapper.writeValueAsString(obj), Dataset.class);
        } catch (IOException e) {
            log.error("Error while trying to serialize Dataset: {}", e.getMessage());
            return null;
        }
    };

    public Mono<Dataset> findById(String id) {
        return esRepository.findById(appConfig.getDatasetIndex(), id)
                .flatMap(response -> Mono.just(toDataset(response.getSourceAsMap())));
    }

    public Mono<ScrollPage<Dataset>> findAll(String scrollId, int size, String[] includeFields, String[] excludeFields) {
        return esRepository.scrollSearch(appConfig.getDatasetIndex(), scrollId, includeFields, excludeFields, size)
            .flatMap(searchResponse -> Mono.just(ScrollPage.<Dataset>builder()
                .data(Arrays.stream(searchResponse.getHits().getHits())
                    .map(documentFields -> toDataset(documentFields.getSourceAsMap()))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList()))
                .pageId(searchResponse.getScrollId())
                .total(searchResponse.getHits().getTotalHits().value)
                .build()));
    }
}
