package com.asso.ladoxa.webserver.download.models.storage;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.NoArgsConstructor;

@JsonTypeInfo(use=JsonTypeInfo.Id.DEDUCTION)
@JsonSubTypes( {@JsonSubTypes.Type(S3Storage.class)})
@NoArgsConstructor
public abstract class Storage {
}
