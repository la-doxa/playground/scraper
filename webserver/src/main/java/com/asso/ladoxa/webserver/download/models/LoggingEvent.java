package com.asso.ladoxa.webserver.download.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoggingEvent<T> {
    private String message;
    private T payload;
    @JsonProperty("trace.id")
    private String traceId;
    @JsonProperty("@timestamp")
    private String timestamp;
}
