package com.asso.ladoxa.webserver.download;

import reactor.core.publisher.Mono;

import java.nio.file.Path;

public interface IStorageService {
    Mono<Boolean> store(Path path);
}
