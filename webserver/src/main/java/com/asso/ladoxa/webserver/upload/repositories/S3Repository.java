package com.asso.ladoxa.webserver.upload.repositories;

import com.asso.ladoxa.opentracing.NewSpan;
import com.asso.ladoxa.webserver.download.DownloadService;
import com.asso.ladoxa.webserver.download.models.DownloadEvent;
import io.opentracing.Tracer;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import software.amazon.awssdk.auth.credentials.AwsSessionCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import java.net.URI;
import java.nio.file.Paths;

@Log4j2
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class S3Repository {

    private final DownloadService downloadService;
    private Tracer tracer;

    private S3Client s3Client(DownloadEvent downloadEvent) {
        AwsSessionCredentials awsCreds = AwsSessionCredentials.create(
            downloadEvent.getS3().getAccessKeyId(),
            downloadEvent.getS3().getSecretAccessKey(),
            "");
        return S3Client.builder()
            .endpointOverride(URI.create(downloadEvent.getS3().getEndpoint()))
            .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
            .region(Region.of(downloadEvent.getS3().getRegion()))
            .build();
    }

    /**
     * Upload donwloadEvent.filename to downloadEvent.s3 remote storage
     * @param downloadEvent
     * @return DownloadEvent
     */
    @NewSpan
    public Mono<DownloadEvent> uploadToS3(DownloadEvent downloadEvent) {
        return Mono
            .create(sink -> {
                S3Client s3Client = s3Client(downloadEvent);
                PutObjectResponse response = s3Client.putObject(PutObjectRequest.builder()
                    .bucket(downloadEvent.getS3().getBucket())
                    .key(Paths.get(downloadEvent.getS3().getPath(), downloadService.getDestination(downloadEvent)
                        .getFileName().toString()).toString())
                    .build(), RequestBody.fromFile(downloadService.getDestination(downloadEvent)));
                sink.success(downloadEvent);
            });
    }


}
