package com.asso.ladoxa.webserver.download;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties("download")
public class DownloadConfig {
    private String tmpDir;
}
