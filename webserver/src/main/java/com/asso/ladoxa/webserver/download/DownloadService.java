package com.asso.ladoxa.webserver.download;

import com.asso.ladoxa.elasticsearch.ElasticsearchRepository;
import com.asso.ladoxa.opentracing.NewSpan;
import com.asso.ladoxa.webserver.WebServerApplicationConfig;
import com.asso.ladoxa.webserver.download.models.DownloadEvent;
import com.asso.ladoxa.webserver.subscribes.models.ScrollPage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.action.update.UpdateResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DownloadService {

    private final DownloadConfig downloadConfig;
    private final ElasticsearchRepository esRepository;
    private final WebServerApplicationConfig webServerApplicationConfig;
    private final ObjectMapper mapper;

    @PostConstruct
    public void init() {
        try {
            Files.createDirectories(Paths.get(downloadConfig.getTmpDir()));
        } catch(IOException e) {
            log.error("Temp folder creation failed: {}", e.getMessage());
            System.exit(1);
        }
    }

    @PreDestroy
    public void clean() {
        try {
            Files.delete(Paths.get(downloadConfig.getTmpDir()));
        } catch (IOException e) {
            log.error("Temp folder cleaning failed: {}", e.getMessage());
            System.exit(1);
        }
    }

    public Path getDestination(DownloadEvent downloadEvent) {
        return Paths.get(downloadConfig.getTmpDir(), downloadEvent.getFileName());
    }

    @NewSpan
    public Mono<DownloadEvent> download(DownloadEvent downloadEvent) {
        downloadEvent.setStartedAt(Instant.now().toEpochMilli());
        String destination = getDestination(downloadEvent).toString();
        return upsert(downloadEvent)
            .flatMap(updateResponse -> Mono.create(sink -> {
            try {
                URL url = new URL(downloadEvent.getUrl());
                ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());
                FileOutputStream fileOutputStream = new FileOutputStream(destination);
                fileOutputStream.getChannel()
                    .transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
                log.info(downloadEvent.asLoggingEvent("Downloaded"));
                sink.success(downloadEvent);
            } catch (IOException e) {
                sink.error(e);
            }
        }));
    }

    public DownloadEvent toDownloadEvent(Object obj) {
        try {
            return mapper.readValue(mapper.writeValueAsString(obj), new TypeReference<DownloadEvent>(){});
        } catch (IOException e) {
            log.error("Could not deserialize DownloadEvent: {}", e.getMessage());
            return null;
        }
    }

    public Mono<UpdateResponse> upsert(DownloadEvent downloadEvent) {
        return Mono.<String>create(sink -> {
            try {
                sink.success(mapper.writeValueAsString(downloadEvent));
            } catch (JsonProcessingException e) {
                sink.error(e);
            }
        }).flatMap(valueAsString -> esRepository.upsertMono(webServerApplicationConfig.getDownloadEventIndex(),
            downloadEvent.getTraceId(), valueAsString));
    }

    @NewSpan
    public Mono<ScrollPage<DownloadEvent>> findAll(String scrollId, int size, String[] includeFields,
                                                                 String[] excludeFields) {
        return esRepository.scrollSearch(webServerApplicationConfig.getDownloadEventIndex(),
                scrollId, includeFields, excludeFields, size)
            .flatMap(searchResponse -> Mono.just(ScrollPage.<DownloadEvent>builder()
                .data(Arrays.stream(searchResponse.getHits().getHits())
                    .map(documentFields -> toDownloadEvent(documentFields.getSourceAsMap()))
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList()))
                .pageId(searchResponse.getScrollId())
                .total(searchResponse.getHits().getTotalHits().value)
                .build()));
    }
}
