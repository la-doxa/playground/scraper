package com.asso.ladoxa.webserver.download.models.storage;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import software.amazon.awssdk.regions.Region;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class S3Config {
    private String bucket;
    private String path;
    private Region region = Region.EU_WEST_1;
    private String endpoint;

    private String error;

    @JsonProperty("access_key_id")
    private String accessKeyId;

    @JsonProperty("secret_access_key")
    private String secretAccessKey;

    @JsonProperty("region")
    private void readRegion(String region) {
        this.setRegion(Region.of(region));
    }

    public String getRegion() {
        return region.toString();
    }
}
