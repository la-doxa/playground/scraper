package com.asso.ladoxa.webserver.download.models;

import com.asso.ladoxa.webserver.download.models.storage.S3Config;
import com.asso.ladoxa.webserver.download.models.storage.Storage;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DownloadEvent {

    private String url;

    @JsonProperty("trace_id")
    private String traceId;

    @JsonProperty("started_at")
    private long startedAt;

    @JsonProperty("finished_at")
    private long finishedAt;

    @JsonProperty("file_name")
    private String fileName;

    private List<Storage> storage;

    private String error;

    private S3Config s3;

    public LoggingEvent<DownloadEvent> asLoggingEvent(String message) {
        return LoggingEvent.<DownloadEvent>builder()
            .payload(this)
            .message(message)
            .timestamp(Long.toString(Instant.now().toEpochMilli()))
            .build();
    }
}
