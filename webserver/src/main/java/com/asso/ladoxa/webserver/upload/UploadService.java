package com.asso.ladoxa.webserver.upload;

import com.asso.ladoxa.opentracing.NewSpan;
import com.asso.ladoxa.webserver.download.DownloadService;
import com.asso.ladoxa.webserver.download.models.DownloadEvent;
import com.asso.ladoxa.webserver.download.models.storage.S3Storage;
import com.asso.ladoxa.webserver.download.models.storage.Storage;
import com.asso.ladoxa.webserver.upload.repositories.S3Repository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;

@Log4j2
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UploadService {

    private final S3Repository s3Repository;
    private final DownloadService downloadService;

    /**
     * Upload from a downloadEvent request
     *
     * @param downloadEvent
     * @return
     */
    @NewSpan
    public Mono<DownloadEvent> upload(DownloadEvent downloadEvent) {
        Flux<Storage> storageFlux = Flux.fromIterable(downloadEvent.getStorage());
        return Mono.<DownloadEvent>create(sink -> {
            storageFlux.filter(storage -> storage instanceof S3Storage)
                .map(storage -> (S3Storage) storage)
                .flatMap(s3Storage -> s3Repository.uploadToS3(DownloadEvent.builder()
                        .fileName(downloadEvent.getFileName())
                        .url(downloadEvent.getUrl())
                        .s3(s3Storage.getS3())
                        .build())
                        .doOnError(throwable -> {
                            s3Storage.getS3().setError(throwable.getMessage());
                        }))
                .collectList()
                .onErrorContinue((throwable, o) -> log.error("Could not upload document : {}",
                    throwable.getMessage()))
                .block();
            sink.success(downloadEvent);
        })
        .doOnNext(downloadEvent1 -> downloadEvent.setFinishedAt(Instant.now().toEpochMilli()))
        .doOnNext(downloadService::upsert);
    }

}
