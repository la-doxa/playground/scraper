package com.asso.ladoxa.webserver.download.models.storage;

import lombok.Data;

@Data
public class S3Storage extends Storage {
    private S3Config s3;
}
