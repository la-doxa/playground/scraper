package com.asso.ladoxa.webserver;

import co.elastic.apm.opentracing.ElasticApmTracer;
import io.opentracing.Tracer;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.CorsRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;

@Data
@Log4j2
@Configuration
@ConfigurationProperties(value = "webapp")
public class WebServerApplicationConfig implements WebFluxConfigurer {
    private String datasetIndex;
    private String downloadEventIndex;
    private int defaultScrollSize = 15;
    private String allowedOrigins = "*";

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
            .allowedOrigins(allowedOrigins)
        .allowedMethods("*");
    }

    @Bean
    public Tracer tracer(){
        return new ElasticApmTracer();
    }
}
