package com.asso.ladoxa.webserver.subscribes;

import com.asso.ladoxa.datagouv.models.Dataset;
import com.asso.ladoxa.webserver.WebServerApplicationConfig;
import com.asso.ladoxa.webserver.subscribes.models.ScrollPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping("/datasets")
public class DatasetsController {

    protected DatasetsService datasetsService;
    protected WebServerApplicationConfig appConfig;

    @Autowired
    public DatasetsController(DatasetsService datasetsService, WebServerApplicationConfig appConfig) {
        this.datasetsService = datasetsService;
        this.appConfig = appConfig;
    }

    @GetMapping("{id}")
    public Mono<Dataset> findById(@PathVariable String id) {
        return datasetsService.findById(id);
    }

    @GetMapping
    public Mono<ScrollPage<Dataset>> findAll(@RequestParam(name = "pageId", required = false) String pageId,
                                             @RequestParam(name = "size", required = false) Integer size,
                                             @RequestParam(name = "includeFields", required = false) String includeFields,
                                             @RequestParam(name = "excludeFields", required = false) String excludeFields) {
        String[] toInclude = includeFields == null ? new String[]{} : includeFields.split(",");
        String[] toExclude = excludeFields == null ? new String[]{} : excludeFields.split(",");
        int appliedSize = size == null ? appConfig.getDefaultScrollSize() : size;
        return datasetsService.findAll(pageId, appliedSize, toInclude, toExclude);
    }

//    @PostMapping("{id}/subscribes")
//    public Mono<Dataset> createSubscribe(@PathVariable String id, @RequestBody Subscribes subscribes) {
//        return datasetsService.createSubscribe(id, subscribes);
//    }
}
