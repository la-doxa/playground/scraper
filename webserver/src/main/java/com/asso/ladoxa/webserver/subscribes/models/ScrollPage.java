package com.asso.ladoxa.webserver.subscribes.models;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ScrollPage<T> {
    private List<T> data;
    private String pageId;
    private long total;
}
