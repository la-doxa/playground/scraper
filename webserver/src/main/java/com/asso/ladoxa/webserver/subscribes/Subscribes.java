package com.asso.ladoxa.webserver.subscribes;

import lombok.Data;
import java.util.List;

@Data
public class Subscribes {
    private String datasetId;
    private List<String> resourcesId;
}
