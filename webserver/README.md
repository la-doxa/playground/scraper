# Webserver

http://localhost:8080/swagger-ui.html

## Tracing

1. Download [elastic-apm-agent jar](https://search.maven.org/search?q=g:co.elastic.apm%20AND%20a:elastic-apm-agent)

2. Add these JVM options
```bash
-javaagent:/home/${USER}/apps/elastic-apm-agent-1.19.0.jar -Delastic.apm.service_name=dev-acouty-scraper-webserver -Delastic.apm.server_urls=https://localhost:8200 -Delastic.apm.enable_log_correlation=true -Delastic.apm.secret_token=Z2nt1cI2qy9J02gMZz145Fp9 -Delastic.apm.application_packages=com.asso.ladoxa -Delastic.apm.verify_server_cert=false -Delastic.apm.instrument=true -Delastic.apm.service_name=scraper-webserver
```
