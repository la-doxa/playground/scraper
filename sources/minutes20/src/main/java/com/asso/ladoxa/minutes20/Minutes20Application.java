package com.asso.ladoxa.minutes20;

import com.asso.ladoxa.CommonConfig;
import com.asso.ladoxa.elasticsearch.ElasticsearchRepository;
import com.asso.ladoxa.minutes20.crawler.CrawlerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;

import java.io.IOException;

@Slf4j
@Import({CommonConfig.class})
@SpringBootApplication
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class Minutes20Application implements CommandLineRunner {

    public static final String baseUrl = "https://www.20minutes.fr/politique";
    public static final String url = "https://www.20minutes.fr";

    private final CrawlerService crawlerService;
    private final ElasticsearchRepository esRepository;
    private final RestHighLevelClient esClient;

    public static void main(String[] args) {
        SpringApplication.run(Minutes20Application.class, args);
    }

    @Override
    public void run(String... args) {
        crawlerService.getPages(baseUrl)
                .flatMap(tuple -> crawlerService.prepareUrl(tuple))
                .flatMapMany(Flux::fromIterable)
                .flatMap(url -> crawlerService.request(url), 1)
                .flatMap(html -> crawlerService.getHRef(html))
                .flatMap(element -> crawlerService.getArticle(element), 4)
                .flatMap(tuple -> crawlerService.toArticle(tuple))
                .filter(article -> !StringUtils.isEmpty(article.getId())) // TODO: some articles does not have id
                .flatMap(article -> esRepository.buildIndexRequest("20minutes-articles",
                        article.getId(),
                        article))
                .buffer(100)
                .flatMap(esRepository::buildBulkRequest)
                .doOnNext(requests -> log.info("Bulking {} items", requests.requests().size()))
                .flatMap(esRepository::bulk, 1)
                .onErrorContinue((e,i) -> log.warn(e.getMessage()))
                .doOnComplete(this::closeEsClient)
                .subscribe();
    }

    public void closeEsClient() {
        log.info("Closing Elasticsearch connection.");
        try {
            esClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
