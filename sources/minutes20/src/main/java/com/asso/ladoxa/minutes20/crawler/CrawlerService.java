package com.asso.ladoxa.minutes20.crawler;

import com.asso.ladoxa.minutes20.Minutes20Application;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.util.function.Tuple2;

import java.util.List;

@Slf4j
@Service
public class CrawlerService {

    WebClient baseClient = WebClient.builder()
            .clientConnector(new ReactorClientHttpConnector(
                    HttpClient.create().followRedirect(true)
            ))
            .exchangeStrategies(ExchangeStrategies.builder()
                    .codecs(configurer -> configurer
                            .defaultCodecs()
                            .maxInMemorySize(16 * 1024 * 1024))
                    .build())
            .build();

    public Mono<Tuple2<Integer, Integer>> getPages(String url) {
        log.info(url);
        return baseClient
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(String.class)
                .onErrorContinue((throwable, o) -> log.error("Could not Request" +
                        " {}. Cause: {}", o.toString(), throwable.getMessage()))
                .flatMap(html -> {
                    Document doc = Jsoup.parse(html);
                    Elements elements = doc.select(".pager-list > li");
                    Integer firstPage = Integer.valueOf(String.valueOf(elements.get(0) // get first element which is Page 1
                            .childNode(0) // get span
                            .childNode(1))); // get span value

                    Integer lastPage = Integer.valueOf(String.valueOf(elements.get(elements.size() - 1) // get first element which is last Page
                            .childNode(0) // get span
                            .childNode(1))); // get span value
                    return Mono.zip(Mono.just(firstPage), Mono.just(lastPage));
                });
    }

    public Mono<List<String>> prepareUrl(Tuple2<Integer, Integer> tuple) {
        return Flux.range(tuple.getT1(), tuple.getT2())
                .flatMap(page -> Mono.just(Minutes20Application.baseUrl + "?page=" + page))
                .collectList();
    }

    public Mono<String> request(String url) {
        log.info(url);
        return baseClient
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(String.class);
    }

    public Flux<Element> getHRef(String html) {
        Document doc = Jsoup.parse(html);
        Elements elements = doc.select("article > a");
        return Flux.fromIterable(elements);
    }

    public Mono<Tuple2<String, String>> getArticle(Element element) {
        String href = Minutes20Application.url + element.attr("href");
        log.info(href);
        return baseClient
                .get()
                .uri(href)
                .retrieve()
                .bodyToMono(String.class)
                .onErrorContinue((throwable, o) -> log.error("Could not Request" +
                " {}. Cause: {}", o.toString(), throwable.getMessage()))
                .zipWith(Mono.just(href));
    }

    public Flux<Article> toArticle(Tuple2<String, String> tuple) {
        Document doc = Jsoup.parse(tuple.getT1());
        return Flux.just(new Article("20Minutes",
                doc.select("div[data-content-id]").attr("data-content-id"),
                doc.title(),
                tuple.getT2(),
                doc.select("address.authorsign").text(),
                doc.select("div.nodeheader-infos > p.datetime > time").get(0).attr("datetime"),
                doc.select("div.nodeheader-infos > p.datetime > time").get(1).attr("datetime"),
                doc.select("div.lt-endor-body.content > p").text()));
    }
}
