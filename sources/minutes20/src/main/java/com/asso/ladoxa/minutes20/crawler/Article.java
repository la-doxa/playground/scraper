package com.asso.ladoxa.minutes20.crawler;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Article {
    String publication;
    String id;
    String title;
    String location ;
    String author;
    String publishedAt;
    String updatedAt;
    String content;
}
