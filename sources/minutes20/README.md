# 20minutes scraper

Scrape 20minutes politique articles and create Article.java object :

```
public class Article {
    String publication;
    String id;
    String title;
    String location ;
    String author;
    String publishedAt;
    String updatedAt;
    String content;
}
```

## ES Mapping

```
PUT 20minutes-articles
{
  "mappings" : {
      "properties" : {
        "id" : {
          "type" : "keyword"
        },
        "author" : {
          "type" : "text",
          "analyzer": "french"
        },
        "content" : {
          "type" : "text",
          "analyzer": "french"
        },
        "location" : {
          "type" : "text",
          "analyzer": "french"
        },
        "publication" : {
          "type" : "text",
          "analyzer": "french"
        },
        "publishedAt" : {
          "type" : "date"
        },
        "title" : {
          "type" : "text",
          "analyzer": "french"
        },
        "updatedAt" : {
          "type" : "date"
        }
      }
    }
}
```