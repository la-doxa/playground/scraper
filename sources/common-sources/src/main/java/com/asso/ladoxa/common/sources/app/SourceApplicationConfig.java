package com.asso.ladoxa.common.sources.app;

import com.asso.ladoxa.common.sources.models.Source;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "ladoxa")
public class SourceApplicationConfig {
    @NestedConfigurationProperty
    private Source source;
}
