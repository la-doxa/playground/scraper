package com.asso.ladoxa.common.sources.app;

import com.asso.ladoxa.elasticsearch.ElasticsearchRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Import;

import java.io.IOException;

@Data
@Slf4j
@Import(SourceApplicationConfig.class)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public abstract class SourceApplication implements CommandLineRunner {

    private final ElasticsearchRepository esRepository;
    private final RestHighLevelClient esClient;
    private final SourceApplicationConfig sourceConfig;

    @Override
    public void run(String... args) throws Exception {
    }

    public void closeEsClient() {
        log.info("Closing Elasticsearch connection.");
        try {
            this.getEsClient().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
