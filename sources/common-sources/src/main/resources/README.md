# Sources

Sources gives metadata from where a Resources is created

ie: datagouv, 20minutes are sources

# Resources

Resources gives metadata from actual data

ie : articles from 20minutes and datasets from datagouv are resources

# scraper-resources
```json
{
  "source" : {
    "name": "20minutes",
    "url": "toto",
    "iconUrl": "https://www.20minutes.fr/",
    "description": "Journal français"
  },
  "resource": {
    "title" : "2020-048-ri-alsh.pdf",
    "description": "string",
    "url" : "https://static.data.gouv.fr/resources/2020-deliberations-du-conseil-municipal-de-jouarre/20201215-143652/2020-048-ri-alsh.pdf",
    "filesize": 464853,
    "checksum": "azeaeaze",
    "filetype" : "file",
    "format" : "pdf",
    "created_at" : "2020-12-15T14:36:52.451",
    "updated_at" : "2020-12-15T14:36:52.394",
    "published_at" : "2020-12-15T14:36:52.451",
    "extras" : {
      ... can be anything more specific to the resource...
    }
  },
  "provider": {
    "author": "Yin-kit CHONG",
    "page": "https://www.data.gouv.fr/fr/organizations/agence-technique-departementale-de-la-charente/",
    "email": "@gmail.com",
    "phone": "aeaea"
  }
}
```