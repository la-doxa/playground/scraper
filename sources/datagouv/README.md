# Data gouv source

Generated models from [swagger.json](https://www.data.gouv.fr/api/1/swagger.json) with [swagger-codegen](https://github.com/swagger-api/swagger-codegen)

```
java -jar swagger-codegen-cli-2.4.14.jar generate \
  -i https://www.data.gouv.fr/api/1/swagger.json \
  --api-package com.asso.ladoxa.datagouv.client.api \
  --model-package com.asso.ladoxa.datagouv.client.model \
  --invoker-package com.asso.ladoxa.datagouv.client.invoker \
  --group-id com.asso.ladoxa \
  --artifact-id data-gouv-api-client \
  --artifact-version 0.0.1-SNAPSHOT \
  -l java \
  --library resttemplate \
  -o data-gouv-api-client
```

After generating models
- cleaned up @javax annotations
- Replace OffsetDateTime.class to DateTime.class (JodaTime)

# Model in ES

```json
{
  "data" : {
    "private" : false,
    "acronym" : null,
    "archived" : null,
    "badges" : [ ],
    "community_resources" : null,
    "created_at" : "2020-06-10T16:27:14.951Z",
    "deleted" : null,
    "description" : "this is a description",
    "extras" : { },
    "featured" : null,
    "frequency" : "unknown",
    "frequency_date" : null,
    "id" : "5ee0edc2222a4d9daff1c5bb",
    "last_modified" : "2020-06-10T16:27:39.587Z",
    "last_update" : "2020-06-10T16:27:39.522Z",
    "license" : "odc-odbl",
    "metrics" : {
      "discussions" : 0,
      "followers" : 0,
      "issues" : 0,
      "reuses" : 0,
      "views" : 0
    },
    "organization" : {
      "acronym" : null,
      "class" : "Organization",
      "id" : "53698dada3a729239d20331d",
      "logo" : "https://static.data.gouv.fr/avatars/ac/83f761a23f4263be8b3401b01625e8-original.png",
      "logo_thumbnail" : "https://static.data.gouv.fr/avatars/ac/83f761a23f4263be8b3401b01625e8-100.png",
      "name" : "Cour des comptes",
      "page" : "https://www.data.gouv.fr/fr/organizations/cour-des-comptes/",
      "slug" : "cour-des-comptes",
      "uri" : "https://www.data.gouv.fr/api/1/organizations/cour-des-comptes/"
    },
    "owner" : null,
    "page" : "https://www.data.gouv.fr/fr/datasets/le-pilotage-strategique-des-operateurs-de-laction-exterieure-de-letat/",
    "quality" : null,
    "resources" : [
      {
        "checksum" : null,
        "created_at" : "2020-06-10T16:27:39.522000",
        "description" : null,
        "extras" : {
          "check:available" : true,
          "check:count-availability" : 50,
          "check:date" : "2020-06-28T09:22:43.287000",
          "check:headers:content-length" : 17926,
          "check:headers:content-type" : "application/zip",
          "check:status" : 200,
          "check:url" : "https://www.ccomptes.fr/system/files/2020-06/20200610-donnees-pilotage-MEAE-operateurs-action-exterieure-Etat.zip"
        },
        "filesize" : null,
        "filetype" : "remote",
        "format" : "csv/utf8",
        "id" : "29fa5e54-c82a-414a-a10c-f7d13482c364",
        "last_modified" : "2020-06-10T16:27:39.522000",
        "latest" : "https://www.data.gouv.fr/fr/datasets/r/29fa5e54-c82a-414a-a10c-f7d13482c364",
        "metrics" : {
          "views" : 1
        },
        "mime" : null,
        "preview_url" : null,
        "published" : "2020-06-10T16:27:39.522000",
        "title" : "Le pilotage stratégique des opérateurs de l'action extérieure de l'État",
        "type" : "main",
        "url" : "https://www.ccomptes.fr/system/files/2020-06/20200610-donnees-pilotage-MEAE-operateurs-action-exterieure-Etat.zip"
      }
    ],
    "slug" : "le-pilotage-strategique-des-operateurs-de-laction-exterieure-de-letat",
    "spatial" : {
      "geom" : null,
      "granularity" : "other",
      "zones" : [
        "country-group:world"
      ]
    },
    "tags" : [
      "diplomatie",
      "meae"
    ],
    "temporal_coverage" : null,
    "title" : "Le pilotage stratégique des opérateurs de l'action extérieure de l'État",
    "uri" : "https://www.data.gouv.fr/api/1/datasets/le-pilotage-strategique-des-operateurs-de-laction-exterieure-de-letat/"
  },
  "subscribes" : [
    {
      "resourceId" : "123456",
      "last_modified" : "2020-06-10T16:28:39.522000",
      "users" : [
        {
          "name" : "ykchong"
        },
        {
          "name" : "couty"
        }
      ]
    }
  ]
}
```
