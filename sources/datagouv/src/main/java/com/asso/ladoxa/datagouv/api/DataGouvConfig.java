package com.asso.ladoxa.datagouv.api;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;

@Configuration
@EnableWebFlux
public class DataGouvConfig {
}
