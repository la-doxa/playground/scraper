package com.asso.ladoxa.datagouv.redis;

import com.asso.ladoxa.datagouv.redis.models.Subscribes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscribeRepository extends CrudRepository<Subscribes, String> {
}