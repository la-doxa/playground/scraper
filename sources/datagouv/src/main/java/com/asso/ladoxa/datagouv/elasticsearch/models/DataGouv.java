package com.asso.ladoxa.datagouv.elasticsearch.models;

import com.asso.ladoxa.datagouv.models.Dataset;
import com.asso.ladoxa.datagouv.redis.models.Subscribes;
import lombok.Data;

import java.util.List;

@Data
public class DataGouv {
    private Dataset data;
    private List<Subscribes> subscribes;
}
