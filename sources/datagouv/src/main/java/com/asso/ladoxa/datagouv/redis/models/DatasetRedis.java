package com.asso.ladoxa.datagouv.redis.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@Data
@AllArgsConstructor
@RedisHash("DatasetRedis")
public class DatasetRedis {
    @Id
    String id;
    String lastModified; // improve Redis Memory to store it as string
}
