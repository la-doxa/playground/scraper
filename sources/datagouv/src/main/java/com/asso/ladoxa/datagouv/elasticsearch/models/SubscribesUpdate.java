package com.asso.ladoxa.datagouv.elasticsearch.models;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SubscribesUpdate {
    private Object subscribes;
}
