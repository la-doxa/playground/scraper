package com.asso.ladoxa.datagouv.redis.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.List;

@Data
public class Subscribes implements Serializable {
    private String resourceId;

    @JsonProperty("last_modified")
    private DateTime lastModified;

    private List<User> users;
}