package com.asso.ladoxa.datagouv.notifier;

import com.asso.ladoxa.datagouv.elasticsearch.models.DataGouv;
import com.asso.ladoxa.datagouv.models.Dataset;
import com.asso.ladoxa.datagouv.redis.DatasetRedisRepository;
import com.asso.ladoxa.datagouv.redis.models.DatasetRedis;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.datatype.joda.JodaMapper;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.get.GetResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Slf4j
@Service
public class NotifierService {

//    protected JodaMapper mapper;
//    protected DatasetRedisRepository datasetRedisRepository;
//    protected Slack slack;
//
//    @Value("${slack.webhook}")
//    String webhook;
//
//    @Autowired
//    public NotifierService(JodaMapper mapper, DatasetRedisRepository datasetRedisRepository,
//                           Slack slack) {
//        this.mapper = mapper;
//        this.datasetRedisRepository = datasetRedisRepository;
//        this.slack = slack;
//    }
//
//    public void notify(Dataset dataset) {
//        Optional<DatasetRedis> optDataset = datasetRedisRepository.findById(dataset.getId());
//        optDataset.ifPresentOrElse(
//                redis -> {
//                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
//                    LocalDateTime lastModifier = LocalDateTime.parse(redis.getLastModified(), formatter);
//                    if (isNew(lastModifier, dataset)) {
//                        datasetRedisRepository.save(new DatasetRedis(dataset.getId(), dataset.getLastModified().toString()));
//                    }
//                },
//                () -> {
//                    SlackModel model = new SlackModel(String.format(
//                            ":sparkles: *New Dataset* :sparkles:\n" +
//                                    "*Title* : %s \n" +
//                                    "*Description* : %s \n" +
//                                    "*Link* : %s \n" +
//                                    "*Last Modified* : %s \n",
//                            dataset.getTitle(), dataset.getDescription(), dataset.getPage(), dataset.getLastModified()));
//                    try {
//                        slack.send(webhook, mapper.writeValueAsString(model));
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    datasetRedisRepository.save(new DatasetRedis(dataset.getId(), dataset.getLastModified().toString()));
//                });
//    }
//
//    public void notify(GetResult result) {
//        try {
//            DataGouv dataGouv = mapper.readValue(result.sourceAsString(), DataGouv.class);
//            Optional<DatasetRedis> optDataset = datasetRedisRepository.findById(dataGouv.getData().getId());
//            optDataset.ifPresentOrElse(
//                    redis -> {
//                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
//                        LocalDateTime lastModifier = LocalDateTime.parse(redis.getLastModified(), formatter);
//                        if (isNew(lastModifier, dataGouv.getData())) {
//                            datasetRedisRepository.save(new DatasetRedis(dataGouv.getData().getId(), dataGouv.getData().getLastModified().toString()));
//                        }
//                    },
//                    () -> {
//                        datasetRedisRepository.save(new DatasetRedis(dataGouv.getData().getId(), dataGouv.getData().getLastModified().toString()));
//                    });
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public boolean isNew(LocalDateTime cachedDataset, Dataset newDataset) {
//        if (cachedDataset.isAfter(newDataset.getLastModified())) {
//            SlackModel model = new SlackModel(String.format(
//                    ":recycle: *Updated Dataset* :recycle \n" +
//                            "*Title* : %s \n" +
//                            "*Description* : %s \n" +
//                            "*Link* : %s \n" +
//                            "*Last Modified* : %s \n",
//                    newDataset.getTitle(), newDataset.getDescription(), newDataset.getPage(), newDataset.getLastModified()));
//            try {
//                slack.send(webhook, mapper.writeValueAsString(model));
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return true;
//        }
//        return false;
//    }

}
