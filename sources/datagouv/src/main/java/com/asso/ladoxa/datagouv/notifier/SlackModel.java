package com.asso.ladoxa.datagouv.notifier;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SlackModel {
    String text;
}
