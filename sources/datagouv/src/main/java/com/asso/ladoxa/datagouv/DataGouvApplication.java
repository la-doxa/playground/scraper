package com.asso.ladoxa.datagouv;

import com.asso.ladoxa.CommonConfig;
import com.asso.ladoxa.common.sources.app.SourceApplication;
import com.asso.ladoxa.common.sources.app.SourceApplicationConfig;
import com.asso.ladoxa.datagouv.api.DataGouvService;
import com.asso.ladoxa.datagouv.models.Dataset;
import com.asso.ladoxa.datagouv.models.DatasetPage;
import com.asso.ladoxa.datagouv.models.Organization;
import com.asso.ladoxa.elasticsearch.ElasticsearchRepository;
import com.asso.ladoxa.common.sources.models.Resource;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import reactor.core.publisher.Flux;

@Slf4j
@Import({CommonConfig.class})
@SpringBootApplication
public class DataGouvApplication extends SourceApplication {

    private final DataGouvService dataGouvService;

    @Autowired
    public DataGouvApplication(ElasticsearchRepository esRepository, RestHighLevelClient esClient,
                               SourceApplicationConfig sourceConfig, DataGouvService dataGouvService) {
        super(esRepository, esClient, sourceConfig);
        this.dataGouvService = dataGouvService;
    }

    public static void main(String[] args) {
        SpringApplication.run(DataGouvApplication.class, args);
    }

    public Resource toLaDoxaResource(Dataset dataset, com.asso.ladoxa.datagouv.models.Resource dgResource) {
        Dataset d = new Dataset();
        d.setId(dataset.getId());
        d.setFrequency(dataset.getFrequency());
        d.setLicense(dataset.getLicense());
        return Resource.builder()
            .dataset(d)
            .organization(dataset.getOrganization())
            .source(getSourceConfig().getSource())
            .checksum(dgResource.getChecksum())
            .createdAt(dgResource.getCreatedAt())
            .description(dgResource.getDescription())
            .extras(dgResource.getExtras())
            .filesize(dgResource.getFilesize())
            .filetype(Resource.FiletypeEnum.fromValue(dgResource.getFiletype().getValue()))
            .remoteId(dgResource.getId())
            .lastModified(dgResource.getLastModified())
            .url(dgResource.getUrl())
            .previewUrl(dgResource.getPreviewUrl())
            .format(dgResource.getFormat())
            .latest(dgResource.getLatest())
            .metrics(dgResource.getMetrics())
            .mime(dgResource.getMime())
            .published(dgResource.getPublished())
            .title(dgResource.getTitle())
            .type(Resource.TypeEnum.fromValue(dgResource.getType().getValue()))
            .build();
    }

    @Override
    public void run(String... args) {
        Flux<Dataset> datasetFlux = dataGouvService.getPages("https://www.data.gouv.fr/api/1/datasets/")
                .flatMap(dataGouvService::prepareUrl)
                .flatMapMany(Flux::fromIterable)
                .flatMap(dataGouvService::getDatasetPage, 1)
                .flatMapIterable(DatasetPage::getData)
            .cache();

        Flux<IndexRequest> resourcesIndexRequest = datasetFlux
            .flatMap(dataset -> Flux.fromIterable(dataset.getResources())
                .map(resource -> toLaDoxaResource(dataset, resource)))
            .flatMap(resource -> getEsRepository().buildIndexRequest("data-gouv-datasets-resources",
                resource.getDataset().getId() + "-" + resource.getRemoteId(),
                resource));

        Flux<IndexRequest> datasetsIndexRequest = datasetFlux.flatMap(resource -> getEsRepository()
            .buildIndexRequest("data-gouv-datasets", resource.getId(), resource));

        Flux.merge(datasetsIndexRequest, resourcesIndexRequest)
                .buffer(100)
                .flatMap(getEsRepository()::buildBulkRequest)
                .doOnNext(requests -> log.info("Bulking {} items", requests.requests().size()))
                .flatMap(getEsRepository()::bulk, 2)
                .onErrorContinue((e, i) -> log.warn(e.getMessage()))
                .doOnComplete(this::closeEsClient)
                .subscribe();
    }
}
