package com.asso.ladoxa.datagouv.redis;

import com.asso.ladoxa.datagouv.redis.models.DatasetRedis;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DatasetRedisRepository extends CrudRepository<DatasetRedis, String> {
}