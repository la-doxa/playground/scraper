package com.asso.ladoxa.datagouv.api;

import com.asso.ladoxa.datagouv.models.DatasetPage;
import com.asso.ladoxa.datagouv.notifier.NotifierService;
import com.asso.ladoxa.elasticsearch.ElasticsearchRepository;
import com.fasterxml.jackson.datatype.joda.JodaMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.util.function.Tuple2;

import java.util.List;

@Slf4j
@Service
public class DataGouvService {
    protected ElasticsearchRepository esRepository;
    protected JodaMapper mapper;
    protected NotifierService notifierService;

    private int pageSize = 100;

    WebClient baseClient = WebClient.builder()
            .clientConnector(new ReactorClientHttpConnector(
                    HttpClient.create().followRedirect(true)
            ))
            .exchangeStrategies(ExchangeStrategies.builder()
                    .codecs(configurer -> configurer
                            .defaultCodecs()
                            .maxInMemorySize(16 * 1024 * 1024))
                    .build())
            .build();

    @Autowired
    public DataGouvService(ElasticsearchRepository esRepository, JodaMapper mapper,
                           NotifierService notifierService) {
        this.esRepository = esRepository;
        this.mapper = mapper;
        this.notifierService = notifierService;
    }

    public Mono<Tuple2<Integer, Integer>> getPages(String url) {
        return baseClient
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(DatasetPage.class)
                .flatMap(datasetPage -> {
                    Integer firstPage = 1;
                    Integer lastPage = Integer.valueOf((int) Math.ceil(datasetPage.getTotal()/pageSize));
                    return Mono.zip(Mono.just(firstPage), Mono.just(lastPage));
                });
    }

    public Mono<List<String>> prepareUrl(Tuple2<Integer, Integer> tuple) {
        return Flux.range(tuple.getT1(), tuple.getT2())
                .flatMap(page -> Mono.just("https://www.data.gouv.fr/api/1/datasets/?page_size="+pageSize+"&page=" + page))
                .collectList();
    }

    public Mono<DatasetPage> getDatasetPage(String url) {
        log.info(url);
        return baseClient
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(DatasetPage.class);
    }
}
