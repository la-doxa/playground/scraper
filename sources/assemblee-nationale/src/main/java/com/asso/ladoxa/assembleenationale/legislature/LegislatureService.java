package com.asso.ladoxa.assembleenationale.legislature;

import org.elasticsearch.action.bulk.BulkResponse;
import reactor.core.publisher.Flux;

public interface LegislatureService {
    Flux<BulkResponse> init();
}
