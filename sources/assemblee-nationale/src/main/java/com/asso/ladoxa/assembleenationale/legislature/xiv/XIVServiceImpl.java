package com.asso.ladoxa.assembleenationale.legislature.xiv;

import com.asso.ladoxa.assembleenationale.legislature.AbstractLegislatureService;
import com.asso.ladoxa.assembleenationale.legislature.LegislatureService;
import com.asso.ladoxa.elasticsearch.ElasticsearchRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.bulk.BulkResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import java.io.File;
import java.util.Iterator;

@Slf4j
@Service
@Profile("XIV")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class XIVServiceImpl extends AbstractLegislatureService implements LegislatureService {

    protected final ElasticsearchRepository esRepository;
    protected ObjectMapper mapper = new ObjectMapper();
    protected String electedXIV = "http://data.assemblee-nationale.fr/static/openData/repository/14/amo/deputes_senateurs_ministres_legislatures_XIV/AMO20_dep_sen_min_tous_mandats_et_organes_XIV.json.zip";

    public Flux<BulkResponse> init() {
        log.info("Running XIV legislature service ...");
        Flux<DataBuffer> dataBufferFlux = this.getUrl(electedXIV);

        File tmpFile = null;
        try {
            tmpFile = this.createTempFile("elected",".zip");
        } catch (Exception e) {
            log.error(e.getMessage());
            return Flux.empty();
        }

        DataBufferUtils
                .write(dataBufferFlux, tmpFile.toPath())
                .block();

        // there is only 1 file
        Flux<JsonNode> entries = this.readZip(tmpFile)
                .flatMap(this::readZipEntries)
                .flatMap(this::toInputStream)
                .flatMap(this::convertToString)
                .flatMap(this::toJsonNode)
                .cache();

         Flux<BulkResponse> organes = Flux.from(entries)
                 .flatMapIterable(node -> toIterable(node.get("export").get("organes").get("organe").elements()))
                 .flatMap(entry -> {
                     ((ObjectNode)entry).put("legislature", "14");
                     return esRepository.buildIndexRequest("legislature-organe-14",
                             entry.get("uid").asText(),
                             entry);
                 })
                 .buffer(100)
                 .flatMap(esRepository::buildBulkRequest)
                 .doOnNext(requests -> log.info("Bulking {} organes", requests.requests().size()))
                 .flatMap(esRepository::bulk, 1)
                 .onErrorContinue((e,i) -> log.warn(e.getMessage()));

        Flux<BulkResponse> actors = Flux.from(entries)
                .flatMapIterable(node -> toIterable(node.get("export").get("acteurs").get("acteur").elements()))
                .flatMap(entry -> {
                    ((ObjectNode)entry).put("legislature", "14");
                    return esRepository.buildIndexRequest("legislature-acteur-14",
                            entry.get("uid").get("#text").asText(),
                            entry);
                })
                .buffer(100)
                .flatMap(esRepository::buildBulkRequest)
                .doOnNext(requests -> log.info("Bulking {} acteurs", requests.requests().size()))
                .flatMap(esRepository::bulk, 1)
                .onErrorContinue((e,i) -> log.warn(e.getMessage()));

        return Flux.merge(organes, actors);
    }

    public static <T> Iterable<T> toIterable(Iterator<T> iterator)
    {
        return () -> iterator;
    }
}
