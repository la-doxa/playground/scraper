package com.asso.ladoxa.assembleenationale.legislature.xv;

import com.asso.ladoxa.assemblee.models.Acteur;
import com.asso.ladoxa.assemblee.models.Organe;
import com.asso.ladoxa.assembleenationale.legislature.AbstractLegislatureService;
import com.asso.ladoxa.assembleenationale.legislature.LegislatureService;
import com.asso.ladoxa.elasticsearch.ElasticsearchRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.bulk.BulkResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@Slf4j
@Service
@Profile({"default", "XV"})
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class XVServiceImpl extends AbstractLegislatureService implements LegislatureService {
    protected final ElasticsearchRepository esRepository;

    protected String electedXV = "http://data.assemblee-nationale.fr/static/openData/repository/15/amo/deputes_senateurs_ministres_legislature/AMO20_dep_sen_min_tous_mandats_et_organes_XV.json.zip";
    protected String votesXV = "http://data.assemblee-nationale.fr/static/openData/repository/15/loi/scrutins/Scrutins_XV.json.zip";

    @Override
    public Flux<BulkResponse> init() {
        log.info("Running XV legislature service ...");
        Flux<BulkResponse> elected = this.registerElected();
        return elected;
    }

    public Map<String, JsonNode> getScrutin() {
        log.info("Get XVe votes");
        Flux<DataBuffer> dataBufferFlux = this.getUrl(votesXV);
        File tmpFile = null;

        try {
            tmpFile = this.createTempFile("votes", ".json.zip");
        } catch (Exception e) {
            log.error(e.getMessage());
            return new HashMap<>();
        }

        DataBufferUtils
                .write(dataBufferFlux, tmpFile.toPath())
                .block();

        Flux<Tuple2<ZipFile, ZipEntry>> entries = this.readZip(tmpFile)
                .flatMap(this::readZipEntries)
                .cache();

        return Flux.from(entries)
                .flatMap(this::toInputStream)
                .flatMap(this::convertToString)
                .flatMap(this::toJsonNode)
                .collectMap(
                        node -> node.get("scrutin").get("uid").asText(),
                        node -> node.get("scrutin"))
                .block();
    }

    public Flux<BulkResponse> registerElected() {
        log.info("Registering XVe elected and organes entities");
        Flux<DataBuffer> dataBufferFlux = this.getUrl(electedXV);
        File tmpFile = null;
        try {
            tmpFile = this.createTempFile("elected", ".zip");
        } catch (Exception e) {
            log.error(e.getMessage());
            return Flux.empty();
        }

        DataBufferUtils
                .write(dataBufferFlux, tmpFile.toPath())
                .block();

        Flux<Tuple2<ZipFile, ZipEntry>> entries = this.readZip(tmpFile)
                .flatMap(this::readZipEntries)
                .cache();

        Flux<Organe> organeFlux = Flux.from(entries)
                .filter(tuple -> tuple.getT2().getName().contains("/organe/"))
                .flatMap(this::toInputStream)
                .flatMap(this::convertToString)
                .flatMap(this::toJsonNode)
                .flatMap(this::toOrgane)
                .cache();

        Flux<BulkResponse> responseOrgane = Flux.from(organeFlux)
                .flatMap(entry -> esRepository.buildIndexRequest("legislature-organe-15",
                        entry.getUid(),
                        entry))
                .buffer(100)
                .flatMap(esRepository::buildBulkRequest)
                .doOnNext(requests -> log.info("Bulking {} organes", requests.requests().size()))
                .flatMap(esRepository::bulk, 1)
                .onErrorContinue((e, i) -> log.warn(e.getMessage()));

//        Map<String, Organe> groupes = Flux.from(organeFlux)
//                .collectMap(
//                        entry -> entry.getUid(),
//                        entry -> entry)
//                .block();
//
//        Map<String, JsonNode> scrutins = this.getScrutin();

        Flux<BulkResponse> actors = Flux.from(entries)
                .filter(tuple -> tuple.getT2().getName().contains("/acteur/"))
                .flatMap(this::toInputStream)
                .flatMap(this::convertToString)
                .flatMap(this::toJsonNode)
                .flatMap(this::toActeur)
//                .flatMap(entry -> transform(entry, groupes, scrutins))
                .flatMap(entry ->
                        esRepository.buildIndexRequest("legislature-acteur-15",
                                entry.getUid(),
                                entry)
                )
                .buffer(100)
                .flatMap(esRepository::buildBulkRequest)
                .doOnNext(requests -> log.info("Bulking {} acteurs", requests.requests().size()))
                .flatMap(esRepository::bulk, 1)
                .onErrorContinue((e, i) -> log.warn(e.getMessage()));

        return Flux.merge(actors, responseOrgane);
    }

//    public Flux<JsonNode> transform(Acteur depute, Map<String, Organe> groupes, Map<String, JsonNode> scrutins) {
//        try {
//            // Create root obj
//            JsonNode jsonRoot = mapper.readTree(root);
//            ObjectNode objRoot = (ObjectNode) jsonRoot;
//
//            objRoot.put("uid", node.get("acteur").get("uid").get("#text").asText());
//            objRoot.set("etatCivil", node.get("acteur").get("etatCivil"));
//
//            // override mandat with organe info
//            ArrayNode nodeMandats = mapper.createArrayNode();
//            if (node.get("acteur").get("mandats").get("mandat").isArray()) {
//                nodeMandats = (ArrayNode) node.get("acteur").get("mandats").get("mandat");
//            } else {
//                nodeMandats.add(node.get("acteur").get("mandats").get("mandat"));
//            }
//
//            for (JsonNode jsonMandat : nodeMandats) {
//                ObjectNode nodeMandat = (ObjectNode) jsonMandat;
//                ObjectNode nodeOrgane = (ObjectNode) groupes.get(nodeMandat.get("organes").get("organeRef").asText());
//
//                ObjectNode objOrgane = mapper.createObjectNode();
//                objOrgane.put("uid", nodeMandat.get("organes").get("organeRef").asText());
//                nodeMandat.set("organe", objOrgane);
//                if (nodeOrgane != null) {
//                    nodeMandat.replace("organe", nodeOrgane);
//                }
//
//                nodeMandat.remove("organes");
//            }
//
//
//            // Create legislature obj
//            JsonNode jsonlegislature = mapper.readTree(legislature);
//            ObjectNode objlegislature = (ObjectNode) jsonlegislature;
//            objlegislature.put("legislature", "15");
//            objlegislature.set("mandats", node.get("acteur").get("mandats").get("mandat"));
//
//            ((ArrayNode) objRoot.get("legislatures")).add(jsonlegislature);
//            return Flux.just(objRoot);
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//        return Flux.empty();
//    }

    public Flux<Acteur> toActeur(JsonNode node) {
        try {
            return Flux.just(mapper.readValue(node.get("acteur").toString(), Acteur.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Flux.empty();
    }

    public Flux<Organe> toOrgane(JsonNode node) {
        try {
            return Flux.just(mapper.readValue(node.get("organe").toString(), Organe.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Flux.empty();
    }
}
