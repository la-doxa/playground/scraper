package com.asso.ladoxa.assembleenationale;

import com.asso.ladoxa.CommonConfig;
import com.asso.ladoxa.assembleenationale.legislature.LegislatureService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Import({CommonConfig.class})
@SpringBootApplication
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ANApplication implements CommandLineRunner {

    private final RestHighLevelClient esClient;
    private final LegislatureService legislatureService;
    public static final List<File> tmpFiles = new ArrayList<File>();


    public static void main(String[] args) {
        SpringApplication.run(ANApplication.class, args);
    }

    @Override
    public void run(String... args) {
        legislatureService.init()
                .doOnComplete(this::closeEsClient)
                .doOnComplete(this::cleanTempDirectory)
                .subscribe();
    }

    public void cleanTempDirectory() {
        for (File tmpFile : tmpFiles) {
            tmpFile.delete();
        }
    }

    public void closeEsClient() {
        log.info("Closing Elasticsearch connection.");
        try {
            esClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
