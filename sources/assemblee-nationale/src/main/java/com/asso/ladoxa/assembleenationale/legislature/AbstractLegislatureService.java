package com.asso.ladoxa.assembleenationale.legislature;

import com.asso.ladoxa.assemblee.models.Acteur;
import com.asso.ladoxa.assemblee.models.Depute;
import com.asso.ladoxa.assemblee.models.Organe;
import com.asso.ladoxa.assembleenationale.ANApplication;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.util.function.Tuple2;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@Slf4j
public class AbstractLegislatureService {
    protected ObjectMapper mapper = new ObjectMapper();
    protected WebClient baseClient = WebClient.builder()
            .clientConnector(new ReactorClientHttpConnector(
                    HttpClient.create().followRedirect(true)
            ))
            .exchangeStrategies(ExchangeStrategies.builder()
                    .codecs(configurer -> configurer
                            .defaultCodecs()
                            .maxInMemorySize(16 * 1024 * 1024))
                    .build())
            .build();

    public Flux<DataBuffer> getUrl(String url) {
        log.info(url);
        return baseClient
                .get()
                .uri(url)
                .accept(MediaType.MULTIPART_FORM_DATA)
                .retrieve()
                .bodyToFlux(DataBuffer.class);
    }

    public File createTempFile(String prefix, String suffix) throws IOException {
        File tempFile = File.createTempFile(prefix, suffix);
        ANApplication.tmpFiles.add(tempFile);
        return tempFile;
    }

    public Flux<ZipFile> readZip(File file) {
        try {
            return Flux.just(new ZipFile(file));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Flux.empty();
    }

    public Flux<Tuple2<ZipFile, ZipEntry>> readZipEntries(ZipFile zipFile) {
        return Flux.fromStream(zipFile.stream())
                .flatMap(entry -> Mono.zip(Mono.just(zipFile), Mono.just(entry)));
    }

    public IndexRequest buildUpdateRequest(String index, String id, JsonNode entry) {
        IndexRequest request = null;
        try {
            request = new IndexRequest()
                    .index(index)
                    .id(id)
                    .source(mapper.writeValueAsString(entry), XContentType.JSON);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return request;
    }

    public Flux<JsonNode> toJsonNode(String entry) {
        try {
            String nilXML = "{\"@xmlns:xsi\": \"http://www.w3.org/2001/XMLSchema-instance\", \"@xsi:nil\": \"true\"}";
            return Flux.just(mapper.readTree(entry.replace(nilXML, "null")));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return Flux.empty();
    }

    public Flux<InputStream> toInputStream(Tuple2<ZipFile, ZipEntry> tuple) {
        try {
            return Flux.just(tuple.getT1().getInputStream(tuple.getT2()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Flux.empty();
    }

    public Flux<String> convertToString(InputStream inputStream) {
        try {
            return Flux.just(new String(inputStream.readAllBytes(), StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Flux.just("");
    }
}
