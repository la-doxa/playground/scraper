# Scrape assemblee nationale data

http://data.assemblee-nationale.fr/

## Configuration

Set **1** profile to determine which Legislature you want to scrape  
Default is XV

```
spring.profiles.active=XV
```

- XV : 15e legislature
- XIV : 14e legislature
- XIII : 13e legislature
- XII : 12e legislature
- XI : 11e legislature

## elected from 15e legislative

http://data.assemblee-nationale.fr/acteurs/deputes-senateurs-ministres

## elected from 14e legislative

http://data.assemblee-nationale.fr/opendata-archives-xive/deputes-senateurs-et-ministres-xive-legislature