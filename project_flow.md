# Version

git checkout -b {branch}
mvn versions:set -DnewVersion=${version}-SNAPSHOT 
mvn clean install
git push origin {branch}

# Git flow

- master : latest version
- tags: release version
- branch: dev version

# Docker flow

- create docker tag `latest` on git `branch` push
- create docker tag `{version}` on git`tag` push


